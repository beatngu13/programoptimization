//
//  Benchmark.hpp
//  ProgramOptimization
//
//  Created by Daniel Kraus on 07.11.15.
//  Copyright © 2015 Daniel Kraus. All rights reserved.
//

#ifndef Benchmark_hpp
#define Benchmark_hpp

#include <array>
#include <chrono>
#include <iostream>
#include <map>
#include <memory>

#include "AlgorithmType.hpp"
#include "FieldType.hpp"
#include "FieldUtil.hpp"
#include "InsertionSort.hpp"
#include "MergeSort.hpp"
#include "MinimumSearch.hpp"
#include "QuickSort.hpp"
#include "SelectionSort.hpp"

class Benchmark {

private:
    std::unique_ptr<std::map<FieldType, std::string>> fieldTypesMap;
    std::unique_ptr<std::map<AlgorithmType, std::string>> algorithmTypesMap;
    
    void clearCache();
    
public:
    Benchmark();
    ~Benchmark();
    
    template <size_t SIZE>
    void run(AlgorithmType algorithmType, std::array<double, SIZE> &field, FieldType fieldType);
    
    template <size_t SIZE>
    void run(AlgorithmType algorithmType, std::array<double, SIZE> &field);
    
    void run(AlgorithmType algorithmType);
    
    void runAll();
    
};

#include "Benchmark.tpp"

#endif /* Benchmark_hpp */
