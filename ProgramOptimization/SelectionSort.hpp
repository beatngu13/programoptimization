//
//  SelectionSort.hpp
//  ProgramOptimization
//
//  Created by Daniel Kraus on 03.11.15.
//  Copyright © 2015 Daniel Kraus. All rights reserved.
//

#ifndef SelectionSort_hpp
#define SelectionSort_hpp

#include <array>

#include "MinimumSearch.hpp"

class SelectionSort {

public:
    /*
     * Selection sort (forwards).
     */
    template <typename T, size_t SIZE>
    static void sort(std::array<T, SIZE> &field) {
        for (size_t i = 0; i < SIZE; i++) {
            size_t minIndex = MinimumSearch::getMin(field, i);
            std::swap(field[minIndex], field[i]);
        }
    }
    
    /*
     * Insertion sort (backwards).
     */
    template <typename T, size_t SIZE>
    static void sortBackwards(std::array<T, SIZE> &field) {
        for (size_t i = 0; i < SIZE; i++) {
            size_t minIndex = MinimumSearch::getMinBackwards(field, i);
            std::swap(field[minIndex], field[i]);
        }
    }
    
};

#endif /* SelectionSort_hpp */
