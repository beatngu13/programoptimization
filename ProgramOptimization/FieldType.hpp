//
//  FieldType.hpp
//  ProgramOptimization
//
//  Created by Daniel Kraus on 12.11.15.
//  Copyright © 2015 Daniel Kraus. All rights reserved.
//

#ifndef FieldType_hpp
#define FieldType_hpp

enum class FieldType {
    ASCENDING,
    DESCENDING,
    RANDOM
};

#endif /* FieldType_hpp */
