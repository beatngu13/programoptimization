//
//  MergeSort.hpp
//  ProgramOptimization
//
//  Created by Daniel Kraus on 15.12.15.
//  Copyright © 2015 Daniel Kraus. All rights reserved.
//

#ifndef MergeSort_hpp
#define MergeSort_hpp

#include <algorithm>
#include <array>

class MergeSort {

friend class QuickSort;
    
private:
    template <typename T, size_t SIZE>
    static void merge(std::array<T, SIZE> &field,
                      std::array<T, SIZE> &temp,
                      size_t left,
                      size_t middle,
                      size_t right) {
        // Copy left half in ascending order.
        for (size_t k = left; k <= middle; k++) {
            temp[k] = field[k];
        }
        
        // Copy right half in descending order.
        for (size_t k = middle + 1, l = right; k <= right; k++, l--) {
            temp[k] = field[l];
        }
        
        // Merge bitonic sorted field in ascending order.
        size_t i = left;
        size_t j = right;
        
        for (size_t k = left; k <= right; k++) {
            field[k] = temp[j] < temp[i] ? temp[j--] : temp[i++];
        }
    }
    
    template <typename T, size_t SIZE>
    static void mergeNatural(std::array<T, SIZE> &field,
                             std::array<T, SIZE> &temp,
                             size_t left,
                             size_t right,
                             bool ascending) {
        // Copy the given bitonic run.
        for (size_t k = left; k <= right; k++) {
            temp[k] = field[k];
        }
        
        // Merge field according to ascending parameter.
        size_t i = left;
        size_t j = right;
        
        if (ascending) {
            for (size_t k = left; k <= right; k++) {
                field[k] = temp[j] < temp[i] ? temp[j--] : temp[i++];
            }
        } else {
            for (size_t k = right; k >= left; k--) {
                field[k] = temp[j] < temp[i] ? temp[j--] : temp[i++];
            }
        }
    }
    
    template <typename T, size_t SIZE>
    static bool moreBitonicRuns(std::array<T, SIZE> &field,
                                std::array<T, SIZE> &temp) {
        size_t mergeOps = 0;
        size_t left = 0;
        bool ascending = true;
        
        /*
         * Run computation:
         *    |----------- Run 1 -----------|--Run 2--|
         *    -----------------------------------------
         *    |    0    |    1    |    0    |    1    |
         *    -----------------------------------------
         *                 i = 1     i = 2     i = 3
         *               !prevDesc !prevDesc prevDesc
         *                currAsc  !currAsc  currAsc
         * => i - 1 marks the end of a run.
         */
        bool prevDesc = false;
        bool currAsc = true;
        
        for (size_t i = 1; i < SIZE; i++) {
            currAsc = field[i - 1] <= field[i];
            
            if (prevDesc && currAsc) {
                MergeSort::mergeNatural(field, temp, left, i - 1, ascending);
                
                mergeOps++;
                left = i;
                ascending = !ascending;
            }
            
            prevDesc = currAsc == false;
        }
        
        MergeSort::mergeNatural(field, temp, left, SIZE - 1, ascending);
        
        // Only one merge operation <=> Last (global) bitonic run has been sorted.
        return ++mergeOps != 1;
    }
    
public:
    /*
     * Merge sort (bottom-up).
     */
    template <typename T, size_t SIZE>
    static void sort(std::array<T, SIZE> &field) {
        auto temp = std::make_unique<std::array<T, SIZE>>();
        
        // Compute subarray size.
        for (size_t subSize = 1; subSize < SIZE; subSize += subSize) {
            // Compute subarray left index.
            for (size_t left = 0; left < SIZE - subSize; left += 2 * subSize) {
                size_t middle = left + subSize - 1;
                size_t right = std::min(left + 2 * subSize - 1, SIZE - 1);
                
                MergeSort::merge(field, *temp, left, middle, right);
            }
        }
    }
    
    /*
     * Merge sort (natural).
     */
    template <typename T, size_t SIZE>
    static void sortNatural(std::array<T, SIZE> &field) {
        if (SIZE <= 1) {
            return;
        }
        
        auto temp = std::make_unique<std::array<T, SIZE>>();
        
        while (MergeSort::moreBitonicRuns(field, *temp));
    }

};

#endif /* MergeSort_hpp */
