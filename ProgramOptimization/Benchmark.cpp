//
//  Benchmark.cpp
//  ProgramOptimization
//
//  Created by Daniel Kraus on 07.11.15.
//  Copyright © 2015 Daniel Kraus. All rights reserved.
//

#include "Benchmark.hpp"

Benchmark::Benchmark() {
    fieldTypesMap = std::make_unique<std::map<FieldType, std::string>>();
    fieldTypesMap->insert(std::pair<FieldType, std::string>(
            FieldType::ASCENDING, "Ascending"));
    fieldTypesMap->insert(std::pair<FieldType, std::string>(
            FieldType::DESCENDING, "Descending"));
    fieldTypesMap->insert(std::pair<FieldType, std::string>(
            FieldType::RANDOM, "Random"));
    
    algorithmTypesMap = std::make_unique<std::map<AlgorithmType, std::string>>();
    algorithmTypesMap->insert(std::pair<AlgorithmType, std::string>(
            AlgorithmType::MINIMUM_SEARCH, "minimum search"));
    algorithmTypesMap->insert(std::pair<AlgorithmType, std::string>(
            AlgorithmType::MINIMUM_SEARCH_BACKWARDS, "minimum search (backwards)"));
    algorithmTypesMap->insert(std::pair<AlgorithmType, std::string>(
            AlgorithmType::SELECTION_SORT, "selection sort"));
    algorithmTypesMap->insert(std::pair<AlgorithmType, std::string>(
            AlgorithmType::SELECTION_SORT_BACKWARDS, "selection sort (backwards)"));
    algorithmTypesMap->insert(std::pair<AlgorithmType, std::string>(
            AlgorithmType::INSERTION_SORT, "insertion sort"));
    algorithmTypesMap->insert(std::pair<AlgorithmType, std::string>(
            AlgorithmType::INSERTION_SORT_GUARD, "insertion sort (guard)"));
    algorithmTypesMap->insert(std::pair<AlgorithmType, std::string>(
            AlgorithmType::MERGE_SORT, "merge sort"));
    algorithmTypesMap->insert(std::pair<AlgorithmType, std::string>(
            AlgorithmType::MERGE_SORT_NATURAL, "merge sort (natural)"));
    algorithmTypesMap->insert(std::pair<AlgorithmType, std::string>(
            AlgorithmType::QUICK_SORT, "quick sort"));
    algorithmTypesMap->insert(std::pair<AlgorithmType, std::string>(
            AlgorithmType::QUICK_SORT_HYBRID, "quick sort (hybrid)"));
}

Benchmark::~Benchmark() {
    fieldTypesMap.reset();
    algorithmTypesMap.reset();
}

void Benchmark::clearCache() {
    // Double = 64 bit = 8 byte.
    // => 8 byte * 2.621.440 = 20.971.520 byte = 20 Mbyte.
    auto field = std::make_unique<std::array<double, 8 * 2621440>>();
    field->fill(1.0);
    field.reset();
}

void Benchmark::run(AlgorithmType algorithmType) {
    // Double = 64 bit = 8 byte, RAM = 8 Gbyte = 8.589.934.592 byte.
    // => 8.589.934.592 byte / 8 byte = 1.073.741.824.
    std::cout << "Run " << algorithmTypesMap->at(algorithmType) << "... \n\n";
    
    // 64.000.
    auto field0 = std::make_unique<std::array<double, 64000>>();
    run(algorithmType, *field0);
    field0.reset();
    
    // 128.000.
    auto field1 = std::make_unique<std::array<double, 128000>>();
    run(algorithmType, *field1);
    field1.reset();
    
    // 256.000.
    auto field2 = std::make_unique<std::array<double, 256000>>();
    run(algorithmType, *field2);
    field2.reset();
    
    // 512.000.
    auto field3 = std::make_unique<std::array<double, 512000>>();
    run(algorithmType, *field3);
    field3.reset();
    
    // 1.024.000.
    auto field4 = std::make_unique<std::array<double, 1024000>>();
    run(algorithmType, *field4);
    field4.reset();
    
    // 2.048.000.
    auto field5 = std::make_unique<std::array<double, 2048000>>();
    run(algorithmType, *field5);
    field5.reset();
    
    // 4.096.000.
    auto field6 = std::make_unique<std::array<double, 4096000>>();
    run(algorithmType, *field6);
    field6.reset();
    
    // 8.192.000.
    auto field7 = std::make_unique<std::array<double, 8192000>>();
    run(algorithmType, *field7);
    field7.reset();
    
    // 16.384.000.
    auto field8 = std::make_unique<std::array<double, 16384000>>();
    run(algorithmType, *field8);
    field8.reset();
    
    // 32.768.000.
    auto field9 = std::make_unique<std::array<double, 32768000>>();
    run(algorithmType, *field9);
    field9.reset();
    
    // 65.536.000.
    auto field10 = std::make_unique<std::array<double, 65536000>>();
    run(algorithmType, *field10);
    field10.reset();
    
    // 131.072.000.
    auto field11 = std::make_unique<std::array<double, 131072000>>();
    run(algorithmType, *field11);
    field11.reset();
    
    // 262.144.000.
    auto field12 = std::make_unique<std::array<double, 262144000>>();
    run(algorithmType, *field12);
    field12.reset();
    
    // 524.288.000.
    auto field13 = std::make_unique<std::array<double, 524288000>>();
    run(algorithmType, *field13);
    field13.reset();
    
    // 1.048.576.000.
    auto field14 = std::make_unique<std::array<double, 1048576000>>();
    run(algorithmType, *field14);
    field14.reset();
}

void Benchmark::runAll() {
    std::cout << "Run all benchmarks... \n\n";
    
    for (auto iter = algorithmTypesMap->begin(); iter != algorithmTypesMap->end(); ++iter) {
        run(iter->first);
    }
}
