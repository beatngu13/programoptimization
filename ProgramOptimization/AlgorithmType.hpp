//
//  AlgorithmType.hpp
//  ProgramOptimization
//
//  Created by Daniel Kraus on 12.11.15.
//  Copyright © 2015 Daniel Kraus. All rights reserved.
//

#ifndef AlgorithmType_hpp
#define AlgorithmType_hpp

enum class AlgorithmType {
    MINIMUM_SEARCH,
    MINIMUM_SEARCH_BACKWARDS,
    SELECTION_SORT,
    SELECTION_SORT_BACKWARDS,
    INSERTION_SORT,
    INSERTION_SORT_GUARD,
    MERGE_SORT,
    MERGE_SORT_NATURAL,
    QUICK_SORT,
    QUICK_SORT_HYBRID
};

#endif /* AlgorithmType_hpp */
