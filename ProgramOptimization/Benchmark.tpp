//
//  Benchmark.cpp
//  ProgramOptimization
//
//  Created by Daniel Kraus on 07.11.15.
//  Copyright © 2015 Daniel Kraus. All rights reserved.
//

#include "Benchmark.hpp"

template <size_t SIZE>
void Benchmark::run(AlgorithmType algorithmType, std::array<double, SIZE> &field, FieldType fieldType) {
    clearCache();
    auto start = std::chrono::high_resolution_clock::now();
    
    switch (algorithmType) {
        case AlgorithmType::MINIMUM_SEARCH: {
            MinimumSearch::getMin(field);
            break;
        }
        case AlgorithmType::MINIMUM_SEARCH_BACKWARDS: {
            MinimumSearch::getMinBackwards(field);
            break;
        }
        case AlgorithmType::SELECTION_SORT: {
            SelectionSort::sort(field);
            break;
        }
        case AlgorithmType::SELECTION_SORT_BACKWARDS: {
            SelectionSort::sortBackwards(field);
            break;
        }
        case AlgorithmType::INSERTION_SORT: {
            InsertionSort::sort(field);
            break;
        }
        case AlgorithmType::INSERTION_SORT_GUARD: {
            InsertionSort::sortGuard(field);
            break;
        }
        case AlgorithmType::MERGE_SORT: {
            MergeSort::sort(field);
            break;
        }
        case AlgorithmType::MERGE_SORT_NATURAL: {
            MergeSort::sortNatural(field);
            break;
        }
        case AlgorithmType::QUICK_SORT: {
            QuickSort::sort(field);
            break;
        }
        case AlgorithmType::QUICK_SORT_HYBRID: {
            QuickSort::sortHybrid(field);
            break;
        }
        default:
            // NOP.
            break;
    }
    
    auto end = std::chrono::high_resolution_clock::now();
    long long time;
    std::string endl;
    
    // Measure <=0(n) in ns, =O(nlogn) in µs, >O(nlogn) in ms.
    switch (algorithmType) {
        case AlgorithmType::MINIMUM_SEARCH:             // Fall-through <=O(n)...
        case AlgorithmType::MINIMUM_SEARCH_BACKWARDS: { // ...
            time = std::chrono::duration_cast<std::chrono::nanoseconds>(end - start).count();
            endl = " ns.\n";
            break;
        }
        case AlgorithmType::QUICK_SORT:                 // Fall-through =O(nlogn)...
        case AlgorithmType::QUICK_SORT_HYBRID:          // ...
        case AlgorithmType::MERGE_SORT:                 // ...
        case AlgorithmType::MERGE_SORT_NATURAL: {       // ...
            time = std::chrono::duration_cast<std::chrono::microseconds>(end - start).count();
            endl = " \xC2\xB5s.\n";
            break;
        }
        default: {                                      // Default >O(nlogn).
            time = std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count();
            endl = " ms.\n";
            break;
        }
    }
    
    std::cout << fieldTypesMap->at(fieldType) << ": " << time << endl;
    if (fieldType == FieldType::RANDOM) std::cout << "\n";
}

template <size_t SIZE>
void Benchmark::run(AlgorithmType algorithmType, std::array<double, SIZE> &field) {
    std::cout << "Size of " << SIZE << ".\n";
    
    for (auto iter = fieldTypesMap->begin(); iter != fieldTypesMap->end(); ++iter) {
        FieldType fieldType = iter->first;
        FieldUtil::fillDouble(field, fieldType);
        run(algorithmType, field, fieldType);
    }
}
