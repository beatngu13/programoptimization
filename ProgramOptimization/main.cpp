//
//  main.cpp
//  ProgramOptimization
//
//  Created by Daniel Kraus on 16.10.15.
//  Copyright © 2015 Daniel Kraus. All rights reserved.
//

#include "AlgorithmType.hpp"
#include "Benchmark.hpp"

int main(int argc, const char * argv[]) {
    Benchmark benchmark;
    benchmark.runAll();
    return 0;
}
