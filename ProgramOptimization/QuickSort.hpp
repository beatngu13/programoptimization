//
//  QuickSort.hpp
//  ProgramOptimization
//
//  Created by Daniel Kraus on 15.12.15.
//  Copyright © 2015 Daniel Kraus. All rights reserved.
//

#ifndef QuickSort_hpp
#define QuickSort_hpp

#include <array>
#include <utility>

#include "InsertionSort.hpp"
#include "MergeSort.hpp"

class QuickSort {
    
private:
    template <typename T, size_t SIZE>
    static std::pair<int, int> partition(std::array<T, SIZE> &field,
                                   int left,
                                   int right) {
        int i, j, p, q;
        i = p = left - 1;
        j = q = right;
        bool partitioning = i < j;
        
        int pivotIndex = (left + right) / 2;
        T pivotVal = field[pivotIndex];
        std::swap(field[pivotIndex], field[right]);
        
        /*
         * Phase 1:
         *    ------------------------------------------
         *    | =pivot | <pivot |    | >pivot | =pivot |
         *    ------------------------------------------
         * left        p        i    j        q        right
         */
        while (partitioning) {
            // Move from left to find an element that is >= pivot.
            while (field[++i] < pivotVal);
            
            // Move from right to find an element that is <= pivot.
            while (pivotVal < field[--j] && j > left);
            
            // If pointers i (left) and j (right) haven't crossed.
            if ((partitioning = i < j)) {
                std::swap(field[i], field[j]);
                
                // If the left element equals the pivot, move it to the left end.
                if (field[i] == pivotVal) {
                    std::swap(field[++p], field[i]);
                }
                
                // If the right element equals the pivot, move it to the right end.
                if (pivotVal == field[j]) {
                    std::swap(field[j], field[--q]);
                }
            }
        }
        
        /*
         * Phase 2:
         *    ------------------------------------------
         *    | <pivot |        =pivot        | >pivot |
         *    ------------------------------------------
         * left        j                      i        right
         */
        std::swap(field[i], field[right]);
        j = i - 1;
        i++;
        
        for (int k = left; k < p; k++, j--) {
            std::swap(field[k], field[j]);
        }
        
        for (int k = right - 1; k > q; k--, i++) {
            std::swap(field[i], field[k]);
        }
        
        return std::make_pair(i, j);
    }
    
    template <typename T, size_t SIZE>
    static void sortHybridRecursive(std::array<T, SIZE> &field,
                            std::array<T, SIZE> &temp,
                            int left,
                            int right) {
        if (right - left <= 25) {
            InsertionSort::sort(field, left, right);
            return;
        }
        
        int i, j;
        std::tie(i, j) = QuickSort::partition(field, left, right);
        
        int sizeLeft = j - left;
        int sizeRight = right - i;
        bool worstCase = 10 * sizeLeft < sizeRight || 10 * sizeRight < sizeLeft;
        
        if (worstCase) {
            int middle = (left + right) / 2;
            
            QuickSort::sortHybridRecursive(field, temp, left, middle);
            QuickSort::sortHybridRecursive(field, temp, middle + 1, right);
            MergeSort::merge(field, temp, left, middle, right);
        } else {
            QuickSort::sortHybridRecursive(field, temp, left, j);
            QuickSort::sortHybridRecursive(field, temp, i, right);
        }
    }
    
public:
    /*
     * Quick sort (3-way partitioning).
     */
    template <typename T, size_t SIZE>
    static void sort(std::array<T, SIZE> &field,
                     int left = 0,
                     int right = (int) SIZE - 1) {
        if (right <= left) {
            return;
        }
        
        int i, j;
        std::tie(i, j) = QuickSort::partition(field, left, right);
        
        QuickSort::sort(field, left, j);
        QuickSort::sort(field, i, right);
    }
    
    /*
     * Quick sort (hybrid).
     */
    template <typename T, size_t SIZE>
    static void sortHybrid(std::array<T, SIZE> &field) {
        auto temp = std::make_unique<std::array<T, SIZE>>();
        
        QuickSort::sortHybridRecursive(field, *temp, 0, (int) SIZE - 1);
    }

};

#endif /* QuickSort_hpp */
