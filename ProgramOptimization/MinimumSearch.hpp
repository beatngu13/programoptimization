//
//  MinimumSearch.hpp
//  ProgramOptimization
//
//  Created by Daniel Kraus on 25.10.15.
//  Copyright © 2015 Daniel Kraus. All rights reserved.
//

#ifndef MinimumSearch_hpp
#define MinimumSearch_hpp

#include <array>

class MinimumSearch {
    
public:
    /*
     * Minimum search (forwards).
     */
    template <typename T, size_t SIZE>
    static size_t getMin(std::array<T, SIZE> &field,
                         size_t startIndex = 0) {
        size_t minIndex = startIndex;
        T minVal = field[minIndex];
        
        for (size_t i = minIndex; i < SIZE; i++) {
            if (field[i] < minVal) {
                minIndex = i;
                minVal = field[i];
            }
        }
        
        return minIndex;
    }
    
    /*
     * Insertion sort (backwards).
     */
    template <typename T, size_t SIZE>
    static size_t getMinBackwards(std::array<T, SIZE> &field,
                                  size_t endIndex = 0) {
        size_t minIndex = endIndex;
        T minVal = field[endIndex];
        
        for (size_t i = SIZE; i-- > endIndex; ) {
            if (field[i] < minVal) {
                minIndex = i;
                minVal = field[i];
            }
        }
        
        return minIndex;
    }
    
};

#endif /* MinimumSearch_hpp */
