//
//  FieldUtil.hpp
//  ProgramOptimization
//
//  Created by Daniel Kraus on 04.11.15.
//  Copyright © 2015 Daniel Kraus. All rights reserved.
//

#ifndef FieldUtil_hpp
#define FieldUtil_hpp

#include <array>
#include <iostream>
#include <iterator>
#include <random>
#include <string>

#include "FieldType.hpp"

// TODO: DRY.
class FieldUtil {
    
private:
    static const int RANDOM_RANGE = 8000;
    
public:
    template <size_t SIZE>
    static void fillInt(std::array<int, SIZE> &field, FieldType fieldType) {
        switch (fieldType) {
            case FieldType::ASCENDING:
                for (size_t i = 0; i < SIZE; i++) {
                    field[i] = static_cast<int>(i);
                }
                break;
            
            case FieldType::DESCENDING:
                for (size_t i = 0, val = SIZE - 1; i < SIZE; i++, val--) {
                    field[i] = static_cast<int>(val);
                }
                break;
                
            case FieldType::RANDOM: {
                std::random_device randomDevice;
                std::mt19937 generator(randomDevice());
                std::uniform_int_distribution<int> distribution(-RANDOM_RANGE, RANDOM_RANGE);
                
                for (size_t i = 0; i < SIZE; i++) {
                    field[i] = distribution(generator);
                }
                break;
            }
            default:
                // NOP.
                break;
        }
    }
    
    template <size_t SIZE>
    static void fillDouble(std::array<double, SIZE> &field, FieldType fieldType) {
        switch (fieldType) {
            case FieldType::ASCENDING:
                for (size_t i = 0; i < SIZE; i++) {
                    field[i] = static_cast<double>(i);
                }
                break;
                
            case FieldType::DESCENDING:
                for (size_t i = 0, val = SIZE - 1; i < SIZE; i++, val--) {
                    field[i] = static_cast<double>(val);
                }
                break;
                
            case FieldType::RANDOM: {
                std::random_device randomDevice;
                std::mt19937 generator(randomDevice());
                std::uniform_real_distribution<double> distribution(-RANDOM_RANGE, RANDOM_RANGE);
                
                for (size_t i = 0; i < SIZE; i++) {
                    field[i] = distribution(generator);
                }
                break;
            }
            default:
                // NOP.
                break;
        }
    }
    
    template <size_t SIZE>
    static void fillString(std::array<std::string, SIZE> &field, FieldType fieldType) {
        switch (fieldType) {
            case FieldType::ASCENDING:
                for (size_t i = 0; i < SIZE; i++) {
                    field[i] = std::to_string(i);
                }
                break;
            
            case FieldType::DESCENDING:
                for (size_t i = 0, val = SIZE - 1; i < SIZE; i++, val--) {
                    field[i] = std::to_string(val);
                }
                break;
            
            case FieldType::RANDOM: {
                std::random_device randomDevice;
                std::mt19937 generator(randomDevice());
                std::uniform_int_distribution<int> distribution(-RANDOM_RANGE, RANDOM_RANGE);
                
                for (size_t i = 0; i < SIZE; i++) {
                    field[i] = std::to_string(distribution(generator));
                }
                break;
            }
            default:
                // NOP.
                break;
        }
    }

    template <typename T, size_t SIZE>
    static void print(std::array<T, SIZE> &field) {
        copy(field.begin(), field.end(), std::ostream_iterator<T>(std::cout, "; "));
        std::cout << std::endl;
    }
    
};

#endif /* FieldUtil_hpp */
