//
//  InsertionSort.hpp
//  ProgramOptimization
//
//  Created by Daniel Kraus on 04.11.15.
//  Copyright © 2015 Daniel Kraus. All rights reserved.
//

#ifndef InsertionSort_hpp
#define InsertionSort_hpp

#include <array>

#include "MinimumSearch.hpp"

class InsertionSort {
    
public:
    /*
     * Insertion sort (standard).
     */
    template <typename T, size_t SIZE>
    static void sort(std::array<T, SIZE> &field,
                     int startIndex = 0,
                     int endIndex = (int) SIZE - 1) {
        for (int global = startIndex + 1; global <= endIndex; global++) {
            for (int sorted = global; sorted > startIndex
                    && field[sorted] < field[sorted - 1]; sorted--) {
                std::swap(field[sorted], field[sorted - 1]);
            }
        }
    }
    
    /*
     * Insertion sort (with guard).
     */
    template <typename T, size_t SIZE>
    static void sortGuard(std::array<T, SIZE> &field) {
        size_t minIndex = MinimumSearch::getMin(field);
        std::swap(field[minIndex], field[0]);
        
        for (size_t global = 2; global < SIZE; global++) {
            for (size_t sorted = global; field[sorted] < field[sorted - 1]; sorted--) {
                std::swap(field[sorted], field[sorted - 1]);
            }
        }
    }
    
};

#endif /* InsertionSort_hpp */
