//
//  TestConfiguration.hpp
//  ProgramOptimization
//
//  Created by Daniel Kraus on 07.11.15.
//  Copyright © 2015 Daniel Kraus. All rights reserved.
//

#ifndef TestConfiguration_hpp
#define TestConfiguration_hpp

#include "FieldType.hpp"

static const size_t BIG_FIELD_SIZE = 8000;
static const FieldType FIELD_TYPE = FieldType::RANDOM;

#endif /* TestConfiguration_hpp */
