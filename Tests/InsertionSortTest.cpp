//
//  InsertionSortTest.cpp
//  ProgramOptimization
//
//  Created by Daniel Kraus on 07.11.15.
//  Copyright © 2015 Daniel Kraus. All rights reserved.
//

#include <array>
#include <string>

#include "FieldUtil.hpp"
#include "InsertionSort.hpp"
#include "gtest/gtest.h"
#include "TestConfiguration.hpp"

/*
 * Standard.
 */

TEST(InsertionSortTest, SmallIntegerField) {
    std::array<int, 10> smallActual{{7, 1, 0, 3, 5, 2, 8, 9, 4, 6}};
    std::array<int, 10> smallExpected{{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}};
    
    InsertionSort::sort(smallActual);
    
    for (size_t i = 0; i < 10; i++) {
        EXPECT_EQ(smallExpected[i], smallActual[i]);
    }
}

TEST(InsertionSortTest, BigIntegerField) {
    std::array<int, BIG_FIELD_SIZE> bigActual;
    FieldUtil::fillInt(bigActual, FIELD_TYPE);
    std::array<int, BIG_FIELD_SIZE> bigExpected = bigActual;
    
    InsertionSort::sort(bigActual);
    std::sort(std::begin(bigExpected), std::end(bigExpected));
    
    for (size_t i = 0; i < BIG_FIELD_SIZE; i++) {
        EXPECT_EQ(bigExpected[i], bigActual[i]);
    }
}

TEST(InsertionSortTest, SmallDoubleField) {
    std::array<double, 10> smallActual{{7.1, 1.0, 0.3, 3.5, 5.2, 2.8, 8.9, 9.4, 4.6, 6.7}};
    std::array<double, 10> smallExpected{{0.3, 1.0, 2.8, 3.5, 4.6, 5.2, 6.7, 7.1, 8.9, 9.4}};
    
    InsertionSort::sort(smallActual);
    
    for (size_t i = 0; i < 10; i++) {
        EXPECT_EQ(smallExpected[i], smallActual[i]);
    }
}

TEST(InsertionSortTest, BigDoubleField) {
    std::array<double, BIG_FIELD_SIZE> bigActual;
    FieldUtil::fillDouble(bigActual, FIELD_TYPE);
    std::array<double, BIG_FIELD_SIZE> bigExpected = bigActual;
    
    InsertionSort::sort(bigActual);
    std::sort(std::begin(bigExpected), std::end(bigExpected));
    
    for (size_t i = 0; i < BIG_FIELD_SIZE; i++) {
        EXPECT_EQ(bigExpected[i], bigActual[i]);
    }
}

TEST(InsertionSortTest, SmallStringField) {
    std::array<std::string, 10> smallActual{{"hhhh", "b", "a", "dd", "fff", "cc", "iiiii", "jjjjj", "eee", "gggg"}};
    std::array<std::string, 10> smallExpected{{"a", "b", "cc", "dd", "eee", "fff", "gggg", "hhhh", "iiiii", "jjjjj"}};
    
    InsertionSort::sort(smallActual);
    
    for (size_t i = 0; i < 10; i++) {
        EXPECT_EQ(smallExpected[i], smallActual[i]);
    }
}

TEST(InsertionSortTest, BigStringField) {
    std::array<std::string, BIG_FIELD_SIZE> bigActual;
    FieldUtil::fillString(bigActual, FIELD_TYPE);
    std::array<std::string, BIG_FIELD_SIZE> bigExpected = bigActual;
    
    InsertionSort::sort(bigActual);
    std::sort(std::begin(bigExpected), std::end(bigExpected));
    
    for (size_t i = 0; i < BIG_FIELD_SIZE; i++) {
        EXPECT_EQ(bigExpected[i], bigActual[i]);
    }
}

/*
 * Guard.
 */

TEST(InsertionSortTest, SmallIntegerFieldGuard) {
    std::array<int, 10> smallActual{{7, 1, 0, 3, 5, 2, 8, 9, 4, 6}};
    std::array<int, 10> smallExpected{{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}};
    
    InsertionSort::sortGuard(smallActual);
    
    for (size_t i = 0; i < 10; i++) {
        EXPECT_EQ(smallExpected[i], smallActual[i]);
    }
}

TEST(InsertionSortTest, BigIntegerFieldGuard) {
    std::array<int, BIG_FIELD_SIZE> bigActual;
    FieldUtil::fillInt(bigActual, FIELD_TYPE);
    std::array<int, BIG_FIELD_SIZE> bigExpected = bigActual;
    
    InsertionSort::sortGuard(bigActual);
    std::sort(std::begin(bigExpected), std::end(bigExpected));
    
    for (size_t i = 0; i < BIG_FIELD_SIZE; i++) {
        EXPECT_EQ(bigExpected[i], bigActual[i]);
    }
}

TEST(InsertionSortTest, SmallDoubleFieldGuard) {
    std::array<double, 10> smallActual{{7.1, 1.0, 0.3, 3.5, 5.2, 2.8, 8.9, 9.4, 4.6, 6.7}};
    std::array<double, 10> smallExpected{{0.3, 1.0, 2.8, 3.5, 4.6, 5.2, 6.7, 7.1, 8.9, 9.4}};
    
    InsertionSort::sortGuard(smallActual);
    
    for (size_t i = 0; i < 10; i++) {
        EXPECT_EQ(smallExpected[i], smallActual[i]);
    }
}

TEST(InsertionSortTest, BigDoubleFieldGuard) {
    std::array<double, BIG_FIELD_SIZE> bigActual;
    FieldUtil::fillDouble(bigActual, FIELD_TYPE);
    std::array<double, BIG_FIELD_SIZE> bigExpected = bigActual;
    
    InsertionSort::sortGuard(bigActual);
    std::sort(std::begin(bigExpected), std::end(bigExpected));
    
    for (size_t i = 0; i < BIG_FIELD_SIZE; i++) {
        EXPECT_EQ(bigExpected[i], bigActual[i]);
    }
}

TEST(InsertionSortTest, SmallStringFieldGuard) {
    std::array<std::string, 10> smallActual{{"hhhh", "b", "a", "dd", "fff", "cc", "iiiii", "jjjjj", "eee", "gggg"}};
    std::array<std::string, 10> smallExpected{{"a", "b", "cc", "dd", "eee", "fff", "gggg", "hhhh", "iiiii", "jjjjj"}};
    
    InsertionSort::sortGuard(smallActual);
    
    for (size_t i = 0; i < 10; i++) {
        EXPECT_EQ(smallExpected[i], smallActual[i]);
    }
}

TEST(InsertionSortTest, BigStringFieldGuard) {
    std::array<std::string, BIG_FIELD_SIZE> bigActual;
    FieldUtil::fillString(bigActual, FIELD_TYPE);
    std::array<std::string, BIG_FIELD_SIZE> bigExpected = bigActual;
    
    InsertionSort::sortGuard(bigActual);
    std::sort(std::begin(bigExpected), std::end(bigExpected));
    
    for (size_t i = 0; i < BIG_FIELD_SIZE; i++) {
        EXPECT_EQ(bigExpected[i], bigActual[i]);
    }
}

/*
 * Special cases.
 */

TEST(InsertionSortTest, EmptyField) {
    std::array<double, 0> empty;
    InsertionSort::sort(empty);
    ASSERT_TRUE(true); // Must not crash and reach line.
}

TEST(InsertionSortTest, EmptyFieldGuard) {
    std::array<double, 0> empty;
    InsertionSort::sortGuard(empty);
    ASSERT_TRUE(true); // Must not crash and reach line.
}

TEST(InsertionSortTest, SingleField) {
    std::array<double, 1> singleActual{{0.0}};
    std::array<double, 1> singleExpected = singleActual;
    
    InsertionSort::sort(singleActual);
    std::sort(std::begin(singleExpected), std::end(singleExpected));
    
    ASSERT_EQ(singleExpected[0], singleActual[0]);
}

TEST(InsertionSortTest, SingleFieldGuard) {
    std::array<double, 1> singleActual{{0.0}};
    std::array<double, 1> singleExpected = singleActual;
    
    InsertionSort::sortGuard(singleActual);
    std::sort(std::begin(singleExpected), std::end(singleExpected));
    
    ASSERT_EQ(singleExpected[0], singleActual[0]);
}

TEST(InsertionSortTest, InsertAtBegin) {
    std::array<double, 4> beginActual{{1.0, 2.0, 0.0, 3.0}};
    std::array<double, 4> beginExpected = beginActual;
    
    InsertionSort::sort(beginActual);
    std::sort(std::begin(beginExpected), std::end(beginExpected));
    
    for (size_t i = 0; i < 4; i++) {
        EXPECT_EQ(beginExpected[i], beginActual[i]);
    }
}

TEST(InsertionSortTest, InsertAtBeginGuard) {
    std::array<double, 4> beginActual{{1.0, 2.0, 0.0, 3.0}};
    std::array<double, 4> beginExpected = beginActual;
    
    InsertionSort::sortGuard(beginActual);
    std::sort(std::begin(beginExpected), std::end(beginExpected));
    
    for (size_t i = 0; i < 4; i++) {
        EXPECT_EQ(beginExpected[i], beginActual[i]);
    }
}
