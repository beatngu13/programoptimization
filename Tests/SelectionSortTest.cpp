//
//  SelectionSortTest.cpp
//  ProgramOptimization
//
//  Created by Daniel Kraus on 03.11.15.
//  Copyright © 2015 Daniel Kraus. All rights reserved.
//

#include <array>
#include <string>

#include "FieldUtil.hpp"
#include "gtest/gtest.h"
#include "SelectionSort.hpp"
#include "TestConfiguration.hpp"

/*
 * Standard.
 */

TEST(SelectionSortTest, SmallIntegerField) {
    std::array<int, 10> smallActual{{7, 1, 0, 3, 5, 2, 8, 9, 4, 6}};
    std::array<int, 10> smallExpected{{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}};
    
    SelectionSort::sort(smallActual);
    
    for (size_t i = 0; i < 10; i++) {
        EXPECT_EQ(smallExpected[i], smallActual[i]);
    }
}

TEST(SelectionSortTest, BigIntegerField) {
    std::array<int, BIG_FIELD_SIZE> bigActual;
    FieldUtil::fillInt(bigActual, FIELD_TYPE);
    std::array<int, BIG_FIELD_SIZE> bigExpected = bigActual;
    
    SelectionSort::sort(bigActual);
    std::sort(std::begin(bigExpected), std::end(bigExpected));
    
    for (size_t i = 0; i < BIG_FIELD_SIZE; i++) {
        EXPECT_EQ(bigExpected[i], bigActual[i]);
    }
}

TEST(SelectionSortTest, SmallDoubleField) {
    std::array<double, 10> smallActual{{7.1, 1.0, 0.3, 3.5, 5.2, 2.8, 8.9, 9.4, 4.6, 6.7}};
    std::array<double, 10> smallExpected{{0.3, 1.0, 2.8, 3.5, 4.6, 5.2, 6.7, 7.1, 8.9, 9.4}};
    
    SelectionSort::sort(smallActual);
    
    for (size_t i = 0; i < 10; i++) {
        EXPECT_EQ(smallExpected[i], smallActual[i]);
    }
}

TEST(SelectionSortTest, BigDoubleField) {
    std::array<double, BIG_FIELD_SIZE> bigActual;
    FieldUtil::fillDouble(bigActual, FIELD_TYPE);
    std::array<double, BIG_FIELD_SIZE> bigExpected = bigActual;
    
    SelectionSort::sort(bigActual);
    std::sort(std::begin(bigExpected), std::end(bigExpected));
    
    for (size_t i = 0; i < BIG_FIELD_SIZE; i++) {
        EXPECT_EQ(bigExpected[i], bigActual[i]);
    }
}

TEST(SelectionSortTest, SmallStringField) {
    std::array<std::string, 10> smallActual{{"hhhh", "b", "a", "dd", "fff", "cc", "iiiii", "jjjjj", "eee", "gggg"}};
    std::array<std::string, 10> smallExpected{{"a", "b", "cc", "dd", "eee", "fff", "gggg", "hhhh", "iiiii", "jjjjj"}};
    
    SelectionSort::sort(smallActual);
    
    for (size_t i = 0; i < 10; i++) {
        EXPECT_EQ(smallExpected[i], smallActual[i]);
    }
}

TEST(SelectionSortTest, BigStringField) {
    std::array<std::string, BIG_FIELD_SIZE> bigActual;
    FieldUtil::fillString(bigActual, FIELD_TYPE);
    std::array<std::string, BIG_FIELD_SIZE> bigExpected = bigActual;
    
    SelectionSort::sort(bigActual);
    std::sort(std::begin(bigExpected), std::end(bigExpected));
    
    for (size_t i = 0; i < BIG_FIELD_SIZE; i++) {
        EXPECT_EQ(bigExpected[i], bigActual[i]);
    }
}

/*
 * Backwards.
 */

TEST(SelectionSortTest, SmallIntegerFieldBackwards) {
    std::array<int, 10> smallActual{{7, 1, 0, 3, 5, 2, 8, 9, 4, 6}};
    std::array<int, 10> smallExpected{{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}};
    
    SelectionSort::sortBackwards(smallActual);
    
    for (size_t i = 0; i < 10; i++) {
        EXPECT_EQ(smallExpected[i], smallActual[i]);
    }
}

TEST(SelectionSortTest, BigIntegerFieldBackwards) {
    std::array<int, BIG_FIELD_SIZE> bigActual;
    FieldUtil::fillInt(bigActual, FIELD_TYPE);
    std::array<int, BIG_FIELD_SIZE> bigExpected = bigActual;
    
    SelectionSort::sortBackwards(bigActual);
    std::sort(std::begin(bigExpected), std::end(bigExpected));
    
    for (size_t i = 0; i < BIG_FIELD_SIZE; i++) {
        EXPECT_EQ(bigExpected[i], bigActual[i]);
    }
}

TEST(SelectionSortTest, SmallDoubleFieldBackwards) {
    std::array<double, 10> smallActual{{7.1, 1.0, 0.3, 3.5, 5.2, 2.8, 8.9, 9.4, 4.6, 6.7}};
    std::array<double, 10> smallExpected{{0.3, 1.0, 2.8, 3.5, 4.6, 5.2, 6.7, 7.1, 8.9, 9.4}};
    
    SelectionSort::sortBackwards(smallActual);
    
    for (size_t i = 0; i < 10; i++) {
        EXPECT_EQ(smallExpected[i], smallActual[i]);
    }
}

TEST(SelectionSortTest, BigDoubleFieldBackwards) {
    std::array<double, BIG_FIELD_SIZE> bigActual;
    FieldUtil::fillDouble(bigActual, FIELD_TYPE);
    std::array<double, BIG_FIELD_SIZE> bigExpected = bigActual;
    
    SelectionSort::sortBackwards(bigActual);
    std::sort(std::begin(bigExpected), std::end(bigExpected));
    
    for (size_t i = 0; i < BIG_FIELD_SIZE; i++) {
        EXPECT_EQ(bigExpected[i], bigActual[i]);
    }
}

TEST(SelectionSortTest, SmallStringFieldBackwards) {
    std::array<std::string, 10> smallActual{{"hhhh", "b", "a", "dd", "fff", "cc", "iiiii", "jjjjj", "eee", "gggg"}};
    std::array<std::string, 10> smallExpected{{"a", "b", "cc", "dd", "eee", "fff", "gggg", "hhhh", "iiiii", "jjjjj"}};
    
    SelectionSort::sortBackwards(smallActual);
    
    for (size_t i = 0; i < 10; i++) {
        EXPECT_EQ(smallExpected[i], smallActual[i]);
    }
}

TEST(SelectionSortTest, BigStringFieldBackwards) {
    std::array<std::string, BIG_FIELD_SIZE> bigActual;
    FieldUtil::fillString(bigActual, FIELD_TYPE);
    std::array<std::string, BIG_FIELD_SIZE> bigExpected = bigActual;
    
    SelectionSort::sortBackwards(bigActual);
    std::sort(std::begin(bigExpected), std::end(bigExpected));
    
    for (size_t i = 0; i < BIG_FIELD_SIZE; i++) {
        EXPECT_EQ(bigExpected[i], bigActual[i]);
    }
}

/*
 * Special cases.
 */

TEST(SelectionSortTest, EmptyField) {
    std::array<double, 0> empty;
    SelectionSort::sort(empty);
    ASSERT_TRUE(true); // Must not crash and reach line.
}

TEST(SelectionSortTest, EmptyFieldBackwards) {
    std::array<double, 0> empty;
    SelectionSort::sortBackwards(empty);
    ASSERT_TRUE(true); // Must not crash and reach line.
}

TEST(SelectionSortTest, SingleField) {
    std::array<double, 1> singleActual{{0.0}};
    std::array<double, 1> singleExpected = singleActual;
    
    SelectionSort::sort(singleActual);
    std::sort(std::begin(singleExpected), std::end(singleExpected));
    
    ASSERT_EQ(singleExpected[0], singleActual[0]);
}

TEST(SelectionSortTest, SingleFieldBackwards) {
    std::array<double, 1> singleActual{{0.0}};
    std::array<double, 1> singleExpected = singleActual;
    
    SelectionSort::sortBackwards(singleActual);
    std::sort(std::begin(singleExpected), std::end(singleExpected));
    
    ASSERT_EQ(singleExpected[0], singleActual[0]);
}
