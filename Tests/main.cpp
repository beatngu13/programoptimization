//
//  main.cpp
//  Tests
//
//  Created by Daniel Kraus on 25.10.15.
//  Copyright © 2015 Daniel Kraus. All rights reserved.
//

#include "gtest/gtest.h"

int main(int argc, char * argv[]) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
