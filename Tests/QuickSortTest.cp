//
//  QuickSortTest.cpp
//  ProgramOptimization
//
//  Created by Daniel Kraus on 15.12.15.
//  Copyright © 2015 Daniel Kraus. All rights reserved.
//

#include <array>
#include <string>

#include "FieldUtil.hpp"
#include "gtest/gtest.h"
#include "QuickSort.hpp"
#include "TestConfiguration.hpp"

/*
 * Standard.
 */

TEST(QuickSortTest, SmallIntegerField) {
    std::array<int, 10> smallActual{{7, 1, 0, 3, 5, 2, 8, 9, 4, 6}};
    std::array<int, 10> smallExpected{{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}};
    
    QuickSort::sort(smallActual);
    
    for (size_t i = 0; i < 10; i++) {
        EXPECT_EQ(smallExpected[i], smallActual[i]);
    }
}

TEST(QuickSortTest, BigIntegerField) {
    std::array<int, BIG_FIELD_SIZE> bigActual;
    FieldUtil::fillInt(bigActual, FIELD_TYPE);
    std::array<int, BIG_FIELD_SIZE> bigExpected = bigActual;
    
    QuickSort::sort(bigActual);
    std::sort(std::begin(bigExpected), std::end(bigExpected));
    
    for (size_t i = 0; i < BIG_FIELD_SIZE; i++) {
        EXPECT_EQ(bigExpected[i], bigActual[i]);
    }
}

TEST(QuickSortTest, SmallDoubleField) {
    std::array<double, 10> smallActual{{7.1, 1.0, 0.3, 3.5, 5.2, 2.8, 8.9, 9.4, 4.6, 6.7}};
    std::array<double, 10> smallExpected{{0.3, 1.0, 2.8, 3.5, 4.6, 5.2, 6.7, 7.1, 8.9, 9.4}};
    
    QuickSort::sort(smallActual);
    
    for (size_t i = 0; i < 10; i++) {
        EXPECT_EQ(smallExpected[i], smallActual[i]);
    }
}

TEST(QuickSortTest, BigDoubleField) {
    std::array<double, BIG_FIELD_SIZE> bigActual;
    FieldUtil::fillDouble(bigActual, FIELD_TYPE);
    std::array<double, BIG_FIELD_SIZE> bigExpected = bigActual;
    
    QuickSort::sort(bigActual);
    std::sort(std::begin(bigExpected), std::end(bigExpected));
    
    for (size_t i = 0; i < BIG_FIELD_SIZE; i++) {
        EXPECT_EQ(bigExpected[i], bigActual[i]);
    }
}

TEST(QuickSortTest, SmallStringField) {
    std::array<std::string, 10> smallActual{{"hhhh", "b", "a", "dd", "fff", "cc", "iiiii", "jjjjj", "eee", "gggg"}};
    std::array<std::string, 10> smallExpected{{"a", "b", "cc", "dd", "eee", "fff", "gggg", "hhhh", "iiiii", "jjjjj"}};
    
    QuickSort::sort(smallActual);
    
    for (size_t i = 0; i < 10; i++) {
        EXPECT_EQ(smallExpected[i], smallActual[i]);
    }
}

TEST(QuickSortTest, BigStringField) {
    std::array<std::string, BIG_FIELD_SIZE> bigActual;
    FieldUtil::fillString(bigActual, FIELD_TYPE);
    std::array<std::string, BIG_FIELD_SIZE> bigExpected = bigActual;
    
    QuickSort::sort(bigActual);
    std::sort(std::begin(bigExpected), std::end(bigExpected));
    
    for (size_t i = 0; i < BIG_FIELD_SIZE; i++) {
        EXPECT_EQ(bigExpected[i], bigActual[i]);
    }
}

/*
 * Hybrid.
 */

TEST(QuickSortTest, SmallIntegerFieldHybrid) {
    std::array<int, 10> smallActual{{7, 1, 0, 3, 5, 2, 8, 9, 4, 6}};
    std::array<int, 10> smallExpected{{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}};
    
    QuickSort::sortHybrid(smallActual);
    
    for (size_t i = 0; i < 10; i++) {
        EXPECT_EQ(smallExpected[i], smallActual[i]);
    }
}

TEST(QuickSortTest, BigIntegerFieldHybrid) {
    std::array<int, BIG_FIELD_SIZE> bigActual;
    FieldUtil::fillInt(bigActual, FIELD_TYPE);
    std::array<int, BIG_FIELD_SIZE> bigExpected = bigActual;
    
    QuickSort::sortHybrid(bigActual);
    std::sort(std::begin(bigExpected), std::end(bigExpected));
    
    for (size_t i = 0; i < BIG_FIELD_SIZE; i++) {
        EXPECT_EQ(bigExpected[i], bigActual[i]);
    }
}

TEST(QuickSortTest, SmallDoubleFieldHybrid) {
    std::array<double, 10> smallActual{{7.1, 1.0, 0.3, 3.5, 5.2, 2.8, 8.9, 9.4, 4.6, 6.7}};
    std::array<double, 10> smallExpected{{0.3, 1.0, 2.8, 3.5, 4.6, 5.2, 6.7, 7.1, 8.9, 9.4}};
    
    QuickSort::sortHybrid(smallActual);
    
    for (size_t i = 0; i < 10; i++) {
        EXPECT_EQ(smallExpected[i], smallActual[i]);
    }
}

TEST(QuickSortTest, BigDoubleFieldHybrid) {
    std::array<double, BIG_FIELD_SIZE> bigActual;
    FieldUtil::fillDouble(bigActual, FIELD_TYPE);
    std::array<double, BIG_FIELD_SIZE> bigExpected = bigActual;
    
    QuickSort::sortHybrid(bigActual);
    std::sort(std::begin(bigExpected), std::end(bigExpected));
    
    for (size_t i = 0; i < BIG_FIELD_SIZE; i++) {
        EXPECT_EQ(bigExpected[i], bigActual[i]);
    }
}

TEST(QuickSortTest, SmallStringFieldHybrid) {
    std::array<std::string, 10> smallActual{{"hhhh", "b", "a", "dd", "fff", "cc", "iiiii", "jjjjj", "eee", "gggg"}};
    std::array<std::string, 10> smallExpected{{"a", "b", "cc", "dd", "eee", "fff", "gggg", "hhhh", "iiiii", "jjjjj"}};
    
    QuickSort::sortHybrid(smallActual);
    
    for (size_t i = 0; i < 10; i++) {
        EXPECT_EQ(smallExpected[i], smallActual[i]);
    }
}

TEST(QuickSortTest, BigStringFieldHybrid) {
    std::array<std::string, BIG_FIELD_SIZE> bigActual;
    FieldUtil::fillString(bigActual, FIELD_TYPE);
    std::array<std::string, BIG_FIELD_SIZE> bigExpected = bigActual;
    
    QuickSort::sortHybrid(bigActual);
    std::sort(std::begin(bigExpected), std::end(bigExpected));
    
    for (size_t i = 0; i < BIG_FIELD_SIZE; i++) {
        EXPECT_EQ(bigExpected[i], bigActual[i]);
    }
}

/*
 * Special cases.
 */

TEST(QuickSortTest, EmptyField) {
    std::array<double, 0> empty;
    QuickSort::sort(empty);
    ASSERT_TRUE(true); // Must not crash and reach line.
}

TEST(QuickSortTest, EmptyFieldHybrid) {
    std::array<double, 0> empty;
    QuickSort::sortHybrid(empty);
    ASSERT_TRUE(true); // Must not crash and reach line.
}

TEST(QuickSortTest, SingleField) {
    std::array<double, 1> singleActual{{0.0}};
    std::array<double, 1> singleExpected = singleActual;
    
    QuickSort::sort(singleActual);
    std::sort(std::begin(singleExpected), std::end(singleExpected));
    
    ASSERT_EQ(singleExpected[0], singleActual[0]);
}

TEST(QuickSortTest, SingleFieldHybrid) {
    std::array<double, 1> singleActual{{0.0}};
    std::array<double, 1> singleExpected = singleActual;
    
    QuickSort::sortHybrid(singleActual);
    std::sort(std::begin(singleExpected), std::end(singleExpected));
    
    ASSERT_EQ(singleExpected[0], singleActual[0]);
}
