//
//  MergeSortTest.cpp
//  ProgramOptimization
//
//  Created by Daniel Kraus on 15.12.15.
//  Copyright © 2015 Daniel Kraus. All rights reserved.
//

#include <array>
#include <string>

#include "FieldUtil.hpp"
#include "gtest/gtest.h"
#include "MergeSort.hpp"
#include "TestConfiguration.hpp"

/*
 * Standard.
 */

TEST(MergeSortTest, SmallIntegerField) {
    std::array<int, 10> smallActual{{7, 1, 0, 3, 5, 2, 8, 9, 4, 6}};
    std::array<int, 10> smallExpected{{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}};
    
    MergeSort::sort(smallActual);
    
    for (size_t i = 0; i < 10; i++) {
        EXPECT_EQ(smallExpected[i], smallActual[i]);
    }
}

TEST(MergeSortTest, BigIntegerField) {
    std::array<int, BIG_FIELD_SIZE> bigActual;
    FieldUtil::fillInt(bigActual, FIELD_TYPE);
    std::array<int, BIG_FIELD_SIZE> bigExpected = bigActual;
    
    MergeSort::sort(bigActual);
    std::sort(std::begin(bigExpected), std::end(bigExpected));
    
    for (size_t i = 0; i < BIG_FIELD_SIZE; i++) {
        EXPECT_EQ(bigExpected[i], bigActual[i]);
    }
}

TEST(MergeSortTest, SmallDoubleField) {
    std::array<double, 10> smallActual{{7.1, 1.0, 0.3, 3.5, 5.2, 2.8, 8.9, 9.4, 4.6, 6.7}};
    std::array<double, 10> smallExpected{{0.3, 1.0, 2.8, 3.5, 4.6, 5.2, 6.7, 7.1, 8.9, 9.4}};
    
    MergeSort::sort(smallActual);
    
    for (size_t i = 0; i < 10; i++) {
        EXPECT_EQ(smallExpected[i], smallActual[i]);
    }
}

TEST(MergeSortTest, BigDoubleField) {
    std::array<double, BIG_FIELD_SIZE> bigActual;
    FieldUtil::fillDouble(bigActual, FIELD_TYPE);
    std::array<double, BIG_FIELD_SIZE> bigExpected = bigActual;
    
    MergeSort::sort(bigActual);
    std::sort(std::begin(bigExpected), std::end(bigExpected));
    
    for (size_t i = 0; i < BIG_FIELD_SIZE; i++) {
        EXPECT_EQ(bigExpected[i], bigActual[i]);
    }
}

TEST(MergeSortTest, SmallStringField) {
    std::array<std::string, 10> smallActual{{"hhhh", "b", "a", "dd", "fff", "cc", "iiiii", "jjjjj", "eee", "gggg"}};
    std::array<std::string, 10> smallExpected{{"a", "b", "cc", "dd", "eee", "fff", "gggg", "hhhh", "iiiii", "jjjjj"}};
    
    MergeSort::sort(smallActual);
    
    for (size_t i = 0; i < 10; i++) {
        EXPECT_EQ(smallExpected[i], smallActual[i]);
    }
}

TEST(MergeSortTest, BigStringField) {
    std::array<std::string, BIG_FIELD_SIZE> bigActual;
    FieldUtil::fillString(bigActual, FIELD_TYPE);
    std::array<std::string, BIG_FIELD_SIZE> bigExpected = bigActual;
    
    MergeSort::sort(bigActual);
    std::sort(std::begin(bigExpected), std::end(bigExpected));
    
    for (size_t i = 0; i < BIG_FIELD_SIZE; i++) {
        EXPECT_EQ(bigExpected[i], bigActual[i]);
    }
}

/*
 * Natural.
 */

TEST(MergeSortTest, SmallIntegerFieldNatural) {
    std::array<int, 10> smallActual{{7, 1, 0, 3, 5, 2, 8, 9, 4, 6}};
    std::array<int, 10> smallExpected{{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}};
    
    MergeSort::sortNatural(smallActual);
    
    for (size_t i = 0; i < 10; i++) {
        EXPECT_EQ(smallExpected[i], smallActual[i]);
    }
}

TEST(MergeSortTest, BigIntegerFieldNatural) {
    std::array<int, BIG_FIELD_SIZE> bigActual;
    FieldUtil::fillInt(bigActual, FIELD_TYPE);
    std::array<int, BIG_FIELD_SIZE> bigExpected = bigActual;
    
    MergeSort::sortNatural(bigActual);
    std::sort(std::begin(bigExpected), std::end(bigExpected));
    
    for (size_t i = 0; i < BIG_FIELD_SIZE; i++) {
        EXPECT_EQ(bigExpected[i], bigActual[i]);
    }
}

TEST(MergeSortTest, SmallDoubleFieldNatural) {
    std::array<double, 10> smallActual{{7.1, 1.0, 0.3, 3.5, 5.2, 2.8, 8.9, 9.4, 4.6, 6.7}};
    std::array<double, 10> smallExpected{{0.3, 1.0, 2.8, 3.5, 4.6, 5.2, 6.7, 7.1, 8.9, 9.4}};
    
    MergeSort::sortNatural(smallActual);
    
    for (size_t i = 0; i < 10; i++) {
        EXPECT_EQ(smallExpected[i], smallActual[i]);
    }
}

TEST(MergeSortTest, BigDoubleFieldNatural) {
    std::array<double, BIG_FIELD_SIZE> bigActual;
    FieldUtil::fillDouble(bigActual, FIELD_TYPE);
    std::array<double, BIG_FIELD_SIZE> bigExpected = bigActual;
    
    MergeSort::sortNatural(bigActual);
    std::sort(std::begin(bigExpected), std::end(bigExpected));
    
    for (size_t i = 0; i < BIG_FIELD_SIZE; i++) {
        EXPECT_EQ(bigExpected[i], bigActual[i]);
    }
}

TEST(MergeSortTest, SmallStringFieldNatural) {
    std::array<std::string, 10> smallActual{{"hhhh", "b", "a", "dd", "fff", "cc", "iiiii", "jjjjj", "eee", "gggg"}};
    std::array<std::string, 10> smallExpected{{"a", "b", "cc", "dd", "eee", "fff", "gggg", "hhhh", "iiiii", "jjjjj"}};
    
    MergeSort::sortNatural(smallActual);
    
    for (size_t i = 0; i < 10; i++) {
        EXPECT_EQ(smallExpected[i], smallActual[i]);
    }
}

TEST(MergeSortTest, BigStringFieldNatural) {
    std::array<std::string, BIG_FIELD_SIZE> bigActual;
    FieldUtil::fillString(bigActual, FIELD_TYPE);
    std::array<std::string, BIG_FIELD_SIZE> bigExpected = bigActual;
    
    MergeSort::sortNatural(bigActual);
    std::sort(std::begin(bigExpected), std::end(bigExpected));
    
    for (size_t i = 0; i < BIG_FIELD_SIZE; i++) {
        EXPECT_EQ(bigExpected[i], bigActual[i]);
    }
}

/*
 * Special cases.
 */

TEST(MergeSortTest, EmptyField) {
    std::array<double, 0> empty;
    MergeSort::sort(empty);
    ASSERT_TRUE(true); // Must not crash and reach line.
}

TEST(MergeSortTest, EmptyFieldNatural) {
    std::array<double, 0> empty;
    MergeSort::sortNatural(empty);
    ASSERT_TRUE(true); // Must not crash and reach line.
}

TEST(MergeSortTest, SingleField) {
    std::array<double, 1> singleActual{{0.0}};
    std::array<double, 1> singleExpected = singleActual;
    
    MergeSort::sort(singleActual);
    std::sort(std::begin(singleExpected), std::end(singleExpected));
    
    ASSERT_EQ(singleExpected[0], singleActual[0]);
}

TEST(MergeSortTest, SingleFieldNatural) {
    std::array<double, 1> singleActual{{0.0}};
    std::array<double, 1> singleExpected = singleActual;
    
    MergeSort::sortNatural(singleActual);
    std::sort(std::begin(singleExpected), std::end(singleExpected));
    
    ASSERT_EQ(singleExpected[0], singleActual[0]);
}
