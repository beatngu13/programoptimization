//
//  MinimumSearchTest.cpp
//  ProgramOptimization
//
//  Created by Daniel Kraus on 03.11.15.
//  Copyright © 2015 Daniel Kraus. All rights reserved.
//

#include <array>
#include <string>

#include "FieldUtil.hpp"
#include "gtest/gtest.h"
#include "MinimumSearch.hpp"
#include "TestConfiguration.hpp"

/*
 * Standard.
 */

TEST(MinimumSearchTest, SmallIntegerField) {
    std::array<int, 10> smallField{{7, 1, 0, 3, 5, 2, 8, 9, 4, 6}};
    
    auto expected = 2;
    auto actual = MinimumSearch::getMin(smallField);
    ASSERT_EQ(expected, actual);
}

TEST(MinimumSearchTest, BigIntegerField) {
    std::array<int, BIG_FIELD_SIZE> bigField;
    FieldUtil::fillInt(bigField, FIELD_TYPE);
    
    auto expected = std::min_element(bigField.begin(), bigField.end());
    auto actual = MinimumSearch::getMin(bigField);
    ASSERT_EQ(*expected, bigField[actual]);
}

TEST(MinimumSearchTest, SmallDoubleField) {
    std::array<double, 10> smallField{{7.1, 1.0, 0.3, 3.5, 5.2, 2.8, 8.9, 9.4, 4.6, 6.7}};

    auto expected = 2;
    auto actual = MinimumSearch::getMin(smallField);
    ASSERT_EQ(expected, actual);
}

TEST(MinimumSearchTest, BigDoubleField) {
    std::array<double, BIG_FIELD_SIZE> bigField;
    FieldUtil::fillDouble(bigField, FIELD_TYPE);
    
    auto expected = std::min_element(bigField.begin(), bigField.end());
    auto actual = MinimumSearch::getMin(bigField);
    ASSERT_EQ(*expected, bigField[actual]);
}

TEST(MinimumSearchTest, SmallStringField) {
    std::array<std::string, 10> smallField{{"hhhh", "b", "a", "dd", "fff", "cc", "iiiii", "jjjjj", "eee", "gggg"}};
    
    auto expected = 2;
    auto actual = MinimumSearch::getMin(smallField);
    ASSERT_EQ(expected, actual);
}

TEST(MinimumSearchTest, BigStringField) {
    std::array<std::string, BIG_FIELD_SIZE> bigField;
    FieldUtil::fillString(bigField, FIELD_TYPE);
    
    auto expected = std::min_element(bigField.begin(), bigField.end());
    auto actual = MinimumSearch::getMin(bigField);
    ASSERT_EQ(*expected, bigField[actual]);
}

/*
 * Backwards.
 */

TEST(MinimumSearchTest, SmallIntegerFieldBackwards) {
    std::array<int, 10> smallField{{7, 1, 0, 3, 5, 2, 8, 9, 4, 6}};
    
    auto expected = 2;
    auto actual = MinimumSearch::getMinBackwards(smallField);
    ASSERT_EQ(expected, actual);
}

TEST(MinimumSearchTest, BigIntegerFieldBackwards) {
    std::array<int, BIG_FIELD_SIZE> bigField;
    FieldUtil::fillInt(bigField, FIELD_TYPE);
    
    auto expected = std::min_element(bigField.begin(), bigField.end());
    auto actual = MinimumSearch::getMinBackwards(bigField);
    ASSERT_EQ(*expected, bigField[actual]);
}

TEST(MinimumSearchTest, SmallDoubleFieldBackwards) {
    std::array<double, 10> smallField{{7.1, 1.0, 0.3, 3.5, 5.2, 2.8, 8.9, 9.4, 4.6, 6.7}};
    
    auto expected = 2;
    auto actual = MinimumSearch::getMinBackwards(smallField);
    ASSERT_EQ(expected, actual);
}

TEST(MinimumSearchTest, BigDoubleFieldBackwards) {
    std::array<double, BIG_FIELD_SIZE> bigField;
    FieldUtil::fillDouble(bigField, FIELD_TYPE);
    
    auto expected = std::min_element(bigField.begin(), bigField.end());
    auto actual = MinimumSearch::getMinBackwards(bigField);
    ASSERT_EQ(*expected, bigField[actual]);
}

TEST(MinimumSearchTest, SmallStringFieldBackwards) {
    std::array<std::string, 10> smallField{{"hhhh", "b", "a", "dd", "fff", "cc", "iiiii", "jjjjj", "eee", "gggg"}};
    
    auto expected = 2;
    auto actual = MinimumSearch::getMinBackwards(smallField);
    ASSERT_EQ(expected, actual);
}

TEST(MinimumSearchTest, BigStringFieldBackwards) {
    std::array<std::string, BIG_FIELD_SIZE> bigField;
    FieldUtil::fillString(bigField, FIELD_TYPE);
    
    auto expected = std::min_element(bigField.begin(), bigField.end());
    auto actual = MinimumSearch::getMinBackwards(bigField);
    ASSERT_EQ(*expected, bigField[actual]);
}

/*
 * Special cases.
 */

TEST(MinimumSearchTest, EmptyField) {
    std::array<std::string, 0> emptyField;
    MinimumSearch::getMin(emptyField);
    ASSERT_TRUE(true); // Must not crash and reach line.
}

TEST(MinimumSearchTest, EmptyFieldBackwards) {
    std::array<std::string, 0> emptyField;
    MinimumSearch::getMinBackwards(emptyField);
    ASSERT_TRUE(true); // Must not crash and reach line.
}

TEST(MinimumSearchTest, SingleField) {
    std::array<std::string, 1> singleField;
    FieldUtil::fillString(singleField, FIELD_TYPE);
    
    auto expected = std::min_element(singleField.begin(), singleField.end());
    auto actual = MinimumSearch::getMin(singleField);
    ASSERT_EQ(*expected, singleField[actual]);
}

TEST(MinimumSearchTest, SingleFieldBackwards) {
    std::array<std::string, 1> singleField;
    FieldUtil::fillString(singleField, FIELD_TYPE);
    
    auto expected = std::min_element(singleField.begin(), singleField.end());
    auto actual = MinimumSearch::getMinBackwards(singleField);
    ASSERT_EQ(*expected, singleField[actual]);
}
